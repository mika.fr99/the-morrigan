using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameManager GM;
    public ScoreManager SM;

    private bool _spamSecurity = false;
    void Update()
    {
        if(Input.GetButtonDown("Submit") && !_spamSecurity)
        {
            _spamSecurity = true;
            GM._IsTimeElapsing = true;
            GM.LoadScene(0);
        }
    }
}
